from src.fit import hermite
from src.structs import Waypoint
from src.trajectory import Trajectory
from src import consts
from src import generator
from src.modifiers import tank
from src import io
import matplotlib.pyplot as plt

points = [
    Waypoint(0.38, 1.2, 0),
    Waypoint(4.5, 1.2, 0),
    Waypoint(6, 2.5, 90),
    Waypoint(6, 4, 90),
    Waypoint(4.5, 5, 180),
    Waypoint(3, 5, 180),
    Waypoint(1, 3, 215)
]

traj = Trajectory()
traj = generator.pathfinder_prepare(points, hermite.fit_hermite_cubic, 100, 0.1, 2, 1, 8)
generator.pathfinder_generate(traj)

for s in traj.splines:
    ld = s.spline_deriv_2(-0.01)
    for x in xrange(0, 100):
        d = s.spline_deriv_2(x / 100.0)
        ld = d
