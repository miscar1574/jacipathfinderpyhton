import math
from src.trajectory import Trajectory
import copy


def pathfinder_modify_tank(original, wheelbase_width):
    w = wheelbase_width / 2
    seg = original.segments

    left_seg = []
    right_seg = []

    for i in xrange(0, len(seg)):
        s = seg[i]
        left = copy.deepcopy(s)
        right = copy.deepcopy(s)

        cos_angle = math.cos(s.heading)
        sin_angle = math.sin(s.heading)

        left.x = s.x - (w * sin_angle)
        left.y = s.y + (w * cos_angle)

        if i > 0:
            last = left_seg[i - 1]
            distance = math.sqrt((left.x - last.x)**2 + (left.y - last.y)**2)

            left.position = last.position + distance
            left.velocity = distance / s.dt
            left.acceleration = (left.velocity - last.velocity) / s.dt
            left.jerk = (left.acceleration - last.acceleration) / s.dt

        right.x = s.x + (w * sin_angle)
        right.y = s.y - (w * cos_angle)

        if i > 0:
            last = right_seg[i - 1]
            l = seg[i -1]
            distance = math.sqrt((right.x - last.x)**2 + (right.y - last.y)**2)

            right.position = last.position + distance
            right.velocity = distance / s.dt
            right.acceleration = (right.velocity - last.velocity) / s.dt
            right.jerk = (right.acceleration - last.acceleration) / s.dt

        left_seg.append(left)
        right_seg.append(right)

    left_traj = Trajectory()
    right_traj = Trajectory()

    left_traj.trajectory_copy(left_seg)
    right_traj.trajectory_copy(right_seg)

    return right_traj, left_traj
