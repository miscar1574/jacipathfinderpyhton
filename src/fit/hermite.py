# import spline
import math


# calculate cubic polynomial for waypoints a and b
def fit_hermite_cubic(a, b, s):
    fit_hermite_pre(a, b, s)

    # calculate relative angles of spline waypoints
    a0_delta = math.tan((a.angle - s.angle_offset))
    a1_delta = math.tan((b.angle - s.angle_offset))

    # calculate coefficients for cubic spline polynomial, see https://goo.gl/kwvNSB
    s.a, s.b = [0]*2
    s.c = (a0_delta + a1_delta) / s.knot_distance**2
    s.d = -(2 * a0_delta + a1_delta) / s.knot_distance
    s.e = a0_delta


# calculate quintic polynomial for waypoints a and b
def fit_hermite_quintic(a, b, s):
    fit_hermite_pre(a, b, s)

    # calculate relative angles of spline waypoints
    a0_delta = math.tan(math.radians(a.angle + s.angle_offset))
    a1_delta = math.tan(math.radians(b.angle + s.angle_offset))

    d = s.knot_distance

    # calculate coefficients for cubic spline polynomial, see https://goo.gl/kwvNSB
    s.a = -(3 * (a0_delta + a1_delta)) / d**4
    s.b = (8 * a0_delta + 7 * a1_delta) / d**3
    s.c = -(6 * a0_delta + 4 * a1_delta) / d**2
    s.d = 0
    s.e = a0_delta


# prepare spline class to be calculated
def fit_hermite_pre(a, b, s):
    s.x_offset = a.x
    s.y_offset = a.y

    dx = (b.x - a.x)
    dy = (b.y - a.y)
    delta = math.sqrt(dx**2 + dy**2)
    s.knot_distance = delta
    s.angle_offset = math.atan2(dy, dx)
