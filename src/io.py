CSV_LEADING_STRING = 'dt,x,y,position,velocity,acceleration,jerk,heading\n'


def pathfinder_write_csv(file_name, traj):
    seg = traj.segments

    file = open(file_name, 'w')

    file.write(CSV_LEADING_STRING)

    for i in xrange(0, len(seg)):
        s = seg[i]
        line = '%f,%f,%f,%f,%f,%f,%f,%f\n' % (s.dt, s.x, s.y, s.position, s.velocity, s.acceleration, s.jerk, s.heading)
        file.write(line)

    file.close()


def pathfinder_get_arrays(traj):
    time = 0
    t_ = []
    x_ = []
    y_ = []
    v_ = []
    a_ = []

    for i in xrange(0, len(traj.segments)):
        s = traj.segments[i]
        x_.append(s.x)
        y_.append(s.y)
        v_.append(s.velocity)
        a_.append(s.acceleration)
        time += s.dt
        t_.append(time)

    return x_, y_, v_, a_, t_
