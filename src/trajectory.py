from src import structs
import math
import numpy


class Trajectory(object):
    # init self.variables
    def __init__(self):
        self.path = []
        self.splines = []
        self.spline_lengths = []
        self.segments = []
        self.info = structs.TrajectoryInfo()
        self.config = structs.TrajectoryConfig()

    def setConfig(self, dt, target_velocities, max_a, max_j, src_v, src_theta, dst_pos, dst_v, dst_theta, sample_count):
        self.config = structs.TrajectoryConfig(dt, target_velocities, max_a, max_j,
                                               src_v, src_theta, dst_pos, dst_v, dst_theta, sample_count)

    def trajectory_copy(self, src):
        self.segments = []

        for i in xrange(0, len(src)):
            s = src[i]
            d = structs.Segment(
                dt=s.dt,
                x=s.x,
                y=s.y,
                position=s.position,
                velocity=s.velocity,
                acceleration=s.acceleration,
                jerk=s.jerk,
                heading=s.heading
            )

            self.segments.append(d)

    def trajectory_prepare(self):
        c = self.config

        last_v = 0
        times = []
        accelerations = []
        for i in xrange(0, len(self.splines)):
            length = self.spline_lengths[i]
            v = c.target_velocities[i]
            next_v = c.target_velocities[i + 1] if i + 1 < len(self.splines) else 0

            at1, at2 = 0, 0

            if v > last_v:
                at1 = (v - last_v) / c.max_a
            if v > next_v:
                at2 = (v - next_v) / c.max_a

            sa1 = 0.5 * c.max_a * pow(at1, 2)
            sa2 = 0.5 * c.max_a * pow(at2, 2)

            sv = length - (sa1 + sa2)
            if sv > 0:
                vt = sv / v
                times.append(at1)
                times.append(vt)
                times.append(at2)
                accelerations.append(c.max_a)
                accelerations.append(0)
                accelerations.append(-c.max_a)

            else:
                if v > last_v and v > next_v:
                    at1 = (-last_v + math.sqrt((pow(last_v, 2) + pow(next_v, 2)) / 2 + c.max_a * length)) / c.max_a
                    at2 = (last_v - next_v + c.max_a * at1) / c.max_a
                    times.append(at1)
                    times.append(at2)
                    accelerations.append(c.max_a)
                    accelerations.append(-c.max_a)
                elif last_v < v < next_v:
                    at = (-last_v + math.sqrt(pow(last_v, 2) + 2 * c.max_a * length)) / c.max_a
                    times.append(at)
                    accelerations.append(c.max_a)
                elif last_v > v > next_v:
                    at = (-next_v + math.sqrt(pow(next_v, 2) + 2 * c.max_a * length)) / c.max_a
                    times.append(at)
                    accelerations.append(-c.max_a)

            last_v = v

        size = int(sum(times) / c.dt)
        self.info = structs.TrajectoryInfo(size, c.dt, times, accelerations)

    def trajectory_create(self):
        ret = self.trajectory_find_segments()

        if ret < 0:
            return ret

        d_theta = self.config.dst_theta - self.config.src_theta
        for i in xrange(0, len(self.segments)):
            self.segments[i].heading = self.config.src_theta + d_theta * self.segments[i].position / \
                                       self.segments[len(self.segments) - 1].position

        return 0

    def trajectory_find_segments(self):
        length, dt = self.info.length, self.info.dt
        times, accelerations = self.info.mvmnt_times, self.info.mvmnt_accelerations

        seg = self.segments

        if length < 0:
            # Error
            return -1

        position = 0
        velocity = 0
        for t in xrange(len(times)):
            for i in xrange(int(times[t] / dt)):
                acceleration = accelerations[t]
                velocity += acceleration * dt
                position += velocity * dt

                seg.append(structs.Segment(dt, 0, 0, position, velocity, acceleration, 0, 0))

        self.segments = seg
        return 0
