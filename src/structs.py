class Waypoint(object):
    # init self.variables
    def __init__(self, x, y, angle, velocity=-1):
        self.x = x
        self.y = y
        self.angle = angle
        self.velocity = velocity


class Coord(object):
    # init self.variables
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Segment(object):
    # init self.variables
    def __init__(self, dt, x, y, position, velocity, acceleration, jerk, heading):
        self.dt = dt
        self.x, self.y, self.heading = x, y, heading
        self.position, self.velocity, self.acceleration, self.jerk = position, velocity, acceleration, jerk


class TrajectoryConfig(object):
    # init self.variables
    def __init__(self, dt=0, target_velocities=0, max_a=0, max_j=0, src_v=0, src_theta=0,
                 dst_pos=0, dst_v=0, dst_theta=0, sample_count=0):
        self.dt = dt
        self.target_velocities, self.max_a, self.max_j = target_velocities, max_a, max_j
        self.src_v, self.src_theta = src_v, src_theta
        self.dst_pos, self.dst_v, self.dst_theta = dst_pos, dst_v, dst_theta
        self.sample_count = sample_count


class TrajectoryInfo(object):
    # init self.variables
    def __init__(self, length=0, dt=0, mvmnt_times=0, mvmnt_accelerations=0):
        self.length, self.dt = length, dt
        self.mvmnt_times, self.mvmnt_accelerations = mvmnt_times, mvmnt_accelerations


class TrajectoryCandidate(object):
    # init self.variables
    def __init__(self, saptr, laptr, total_length, length, path_length, info, config):
        self.saptr, self.laptr = saptr, laptr
        self.total_length, self.length, self.path_length = total_length, length, path_length
        self.info, self.config = info, config
