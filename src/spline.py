import math
import structs


class Spline(object):
    # init self.variables
    def __init__(self):
        self.a, self.b, self.c, self.d, self.e = [0]*5
        self.x_offset, self.y_offset, self.angle_offset = [0]*3
        self.knot_distance, self.arc_length = [0]*2

    # return specific coordinate in the spline by it's placement in the graph in percentage
    def spline_coords(self, percentage):
        percentage = max(min(percentage, 1), 0)
        x = percentage * self.knot_distance
        y = (self.a*x + self.b) * x**4 + (self.c*x + self.d) * x**2 + self.e*x

        cos_theta = math.cos(self.angle_offset)
        sin_theta = math.sin(self.angle_offset)

        c = structs.Coord(
            x * cos_theta - y * sin_theta + self.x_offset,
            x * sin_theta + y * cos_theta + self.y_offset
        )

        return c

    # return derivative of spline by it's placement in the graph in percentage
    def spline_deriv(self, percentage):
        x = percentage * self.knot_distance
        deriv = (5*self.a*x + 4*self.b) * x**3 + (3*self.c*x + 2*self.d) * x + self.e
        return deriv

    # return second derivative of spline by it's placement in the graph in percentage
    def spline_deriv_2(self, a, b, c, d, e, k, p):
        x = p * k
        deriv_2 = (5*a*x + 4*b) * x**3 + (3*c*x + 2*d) * x + e
        return deriv_2

    # return the angle of the spline with the x axis in a certain point by it's placement on the graph in percentage
    def spline_angle(self, percentage):
        radians = math.atan(self.spline_deriv(percentage)) + self.angle_offset
        return radians

    def spline_distance(self, sample_count):
        a, b, c, d, e = self.a, self.b, self.c, self.d, self.e
        knot = self.knot_distance

        arc_length, t, dydt = [0.0]*3

        deriv0 = self.spline_deriv_2(a, b, c, d, e, knot, 0)

        last_integrand = math.sqrt(1.0 + deriv0**2) / sample_count

        for i in xrange(0, sample_count):
            t = i * 1.0 / sample_count
            dydt = self.spline_deriv_2(a, b, c, d, e, knot, t)
            integrand = math.sqrt(1 + dydt**2) / sample_count * 1.0
            arc_length += (integrand + last_integrand) / 2.0
            last_integrand = integrand

        a1 = knot * arc_length
        self.arc_length = a1
        return a1

    def spline_progress_for_distance(self, distance, sample_count):
        a, b, c, d, e = self.a, self.b, self.c, self.d, self.e
        knot = self.knot_distance

        arc_length, last_arc_length, t, dydt = [0.0] * 4

        deriv0 = self.spline_deriv_2(a, b, c, d, e, knot, 0)

        last_integrand = math.sqrt(1.0 + deriv0 ** 2) / sample_count

        distance /= knot

        for i in xrange(0, sample_count + 1):
            t = i * 1.0 / sample_count
            dydt = self.spline_deriv_2(a, b, c, d, e, knot, t)
            integrand = math.sqrt(1 + dydt**2) / sample_count * 1.0
            arc_length += (integrand + last_integrand) / 2.0
            if arc_length > distance:
                break
            last_integrand = integrand
            last_arc_length = arc_length

        interpolated = t
        if arc_length is not last_arc_length:
            interpolated += ((distance - last_arc_length)
                             / (arc_length - last_arc_length) - 1) / sample_count * 1.0
        return interpolated
