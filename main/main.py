from src.fit import hermite
from src.structs import Waypoint
from src import consts
from src import generator
from src.modifiers import tank
from src import io
import matplotlib.pyplot as plt

points = [
    # (x, y, angle, velocity)
    Waypoint(0, 1.5, -30, 2),
    Waypoint(6, 0, 0, 1.5),
    Waypoint(8, 2, 90, 2),
    Waypoint(8, 3, 90, 1.5),
    Waypoint(6, 5, 180, 2),
    Waypoint(2, 6, 130, 2)
]

# Waypoints, fit type, sample rate, dt, max a, max j
traj = generator.pathfinder_prepare(points, hermite.fit_hermite_cubic, consts.PATHFINDER_SAMPLES_LOW, 0.025, 2, 10)
generator.pathfinder_generate(traj)

# Wheelbase
right, left = tank.pathfinder_modify_tank(traj, 0.63)

io.pathfinder_write_csv('traj.csv', traj)
io.pathfinder_write_csv('right.csv', right)
io.pathfinder_write_csv('left.csv', left)

x_, y_, v_, a_, t_ = io.pathfinder_get_arrays(traj)
rx_, ry_, rv_ = io.pathfinder_get_arrays(right)[:3]
lx_, ly_, lv_ = io.pathfinder_get_arrays(left)[:3]

plot_xs = []
plot_ys = []
for p in points:
    plot_xs.append(p.x)
    plot_ys.append(p.y)

plt.subplot(211)
plt.plot(plot_xs, plot_ys, 'o', label='data')
plt.plot(x_, y_, label='trajectory')
plt.plot(rx_, ry_, label='right')
plt.plot(lx_, ly_, label='left')
plt.title('Real-World Position')

plt.subplot(212)
plt.plot(t_, v_, label='cruise')
plt.title('Velocity')

plt.show()
