from src import structs
from src.trajectory import Trajectory
from src.spline import Spline
import math


def pathfinder_prepare(path, fit, sample_count, dt, max_acceleration, max_jerk):
    t = Trajectory()

    t.path = path

    if len(path) < 2:
        return -1

    for i in path:
        i.angle = math.radians(i.angle)

    total_length = 0
    vs = []

    for i in xrange(0, len(path) - 1):
        s = Spline()
        fit(path[i], path[i + 1], s)
        dis = s.spline_distance(sample_count)
        t.splines.append(s)
        t.spline_lengths.append(dis)
        vs.append(path[i].velocity)
        total_length += dis

    t.setConfig(dt, vs, max_acceleration, max_jerk, 0, path[0].angle, total_length, 0, path[0].angle, sample_count)

    t.trajectory_prepare()

    return t


def pathfinder_generate(traj):
    status = traj.trajectory_create()
    if status < 0:
        return status

    spline_pos_initial = 0
    spline_i = 0
    for i in xrange(0, len(traj.segments)):
        print 'Progress: %d%%' % ((i * 1.0) / (traj.info.length * 1.0) * 100)
        pos = traj.segments[i].position

        found = False
        while not found:
            pos_relative = pos - spline_pos_initial
            if pos_relative <= traj.spline_lengths[spline_i]:
                si = traj.splines[spline_i]
                percentage = si.spline_progress_for_distance(pos_relative, traj.config.sample_count)
                coords = si.spline_coords(percentage)
                traj.segments[i].heading = si.spline_angle(percentage)
                traj.segments[i].x = coords.x
                traj.segments[i].y = coords.y
                found = True
            elif spline_i < (len(traj.path) - 2):
                spline_pos_initial += traj.spline_lengths[spline_i]
                spline_i += 1
            else:
                si = traj.splines[len(traj.path) - 2]
                traj.segments[i].heading = si.spline_angle(1)
                coords = si.spline_coords(1)
                traj.segments[i].x = coords.x
                traj.segments[i].y = coords.y
                found = 1
    return traj.info.length

